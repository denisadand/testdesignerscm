namespace: testFolder
flow:
  name: flow
  workflow:
    - testFlow:
        do:
          testFolder.testFlow: []
        navigate:
          - SUCCESS: SUCCESS
  results:
    - SUCCESS
extensions:
  graph:
    steps:
      testFlow:
        x: 273
        y: 77.59999084472656
        navigate:
          2ddc686a-fe5a-862e-3bfb-a4b7a94b1a44:
            targetId: 57765ed2-94ec-5db4-6a73-6fb86f8db77b
            port: SUCCESS
    results:
      SUCCESS:
        57765ed2-94ec-5db4-6a73-6fb86f8db77b:
          x: 530
          y: 80
